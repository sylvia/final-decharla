from django.urls import path
from . import views

app_name = 'DeCharlApp'

urlpatterns = [
    path('', views.index_GoRoom, name='index_GoRoom'),
    path('Settings/', views.settings, name='settings'),
    path('Help/', views.page_help, name='help'),
    path('JsonRoom', views.room_json, name='jsonRoom'),
    path('Logout/', views.logout),
    path('<str:room>/', views.showRoom, name='showRoom'),
]
