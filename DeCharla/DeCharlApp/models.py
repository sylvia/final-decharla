from django.db import models
from django.utils import timezone


# -- Vamos a crear los modelos de la aplicación DeCharla


class Room(models.Model):

    roomName = models.CharField(max_length=50, primary_key=True)

    class Meta:
        verbose_name = "Room"
        verbose_name_plural = "Rooms"

    def __str__(self):
        return self.roomName


class Key(models.Model):

    key             = models.CharField(max_length=20, primary_key=True)

    class Meta:
        verbose_name = "Key"
        verbose_name_plural = "Keys"
        
    def __str__(self):
        return self.key


# Vamos a relacionar las tablas de roomMessage y user.
# Un usuario puede tener muchos comentarios, pero un comentario solo puede tener un usuario.
# Por lo que la relación es: 1 user -> N RoomComment.
class User(models.Model):

    userName   = models.CharField(max_length=50, default='Anónimo')
    sizeFont   = models.IntegerField(default=12)
    font       = models.CharField(max_length=50, default='Arial')
    key        = models.ForeignKey(Key, on_delete=models.CASCADE, related_name='cookie', blank=True, null=True)
    session    = models.CharField(max_length=150, primary_key=True)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"
        ordering = ["-userName"]

    def __str__(self):
        return self.userName

# -- Hay que relacionar las tablas Room y RoomComment.
# -- Una sala puede tener muchos comentarios por lo que relacionamos las tablas
# -- como: 1 Room -> N RoomComment.
class RoomMessage(models.Model):

    room        = models.ForeignKey(Room, on_delete=models.CASCADE, related_name='comments', blank=True, null=True)
    content     = models.TextField()
    photo       = models.BooleanField(default=False)
    messageDate = models.DateTimeField(default=timezone.now)
    user        = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments', blank=True, null=True)

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"
        ordering = ["-messageDate"]

    def __str__(self):
        return self.content


class RoomAccess(models.Model):

	room             = models.ForeignKey(Room, on_delete=models.CASCADE, related_name='access')
	userName         = models.ForeignKey(User, on_delete=models.CASCADE, related_name='access')
	lastAccessDate   = models.DateTimeField()

	class Meta:
		verbose_name = "Last Access"
		verbose_name_plural = "Last Accesses"
		ordering = ["-lastAccessDate"]

	def __str__(self):
		return self.lastAccessDate
