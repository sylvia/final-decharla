import datetime
import uuid

from django.http import HttpResponseRedirect
from .models import Key, User
from django.shortcuts import render


class KeyMiddleware:
    def __init__(self, get_response):
        key = "xx34d23"

        # Crea un objeto Key y guarda la contraseña en la base de datos
        key = Key(key=key)
        key.save()
        self.get_response = get_response

    def __call__(self, request):

        # -- Verificar si se ha iniciado sesión.
        if request.COOKIES.get('authorized') and request.COOKIES.get('idUser'):
            try:
                User.objects.get(session=request.COOKIES.get('idUser'))
                return self.get_response(request)
            except User.DoesNotExist:
                pass

        # -- Si no ha iniciado sesión, entonces vamos a ver si ha mandado la contraseña
        if request.method == 'POST':
            if 'key' in request.POST and 'button' in request.POST:
                try:
                    enterKey = Key.objects.get(key=request.POST['key'])
                    response = HttpResponseRedirect(request.path_info)
                    response.set_cookie('authorized', True)
                    response.set_cookie('last_password_time', datetime.datetime.now().isoformat())

                    if request.COOKIES.get('idUser'):
                        idUser = request.COOKIES['idUser']
                    else:
                        idUser = str(uuid.uuid4())
                        response.set_cookie('idUser', idUser)

                    user, created = User.objects.get_or_create(session=idUser)
                    user.key = enterKey
                    user.save()
                    return response

                except Key.DoesNotExist:
                    context = {
                        'isError401': True
                    }
                    return render(request, 'DeCharlApp/key_form.html', context=context, status=401)

        if 'last_key_time' in request.COOKIES:
            last_key_time = request.COOKIES['last_key_time']
            time_since_key = datetime.datetime.now() - datetime.datetime.fromisoformat(last_key_time)
            if time_since_key < datetime.timedelta(hours=2):
                # -- permite el acceso si se proporciona la contraseña en las dos últimas horas
                response = self.get_response(request)
                response.set_cookie('authorized', True)
                return response

        # -- Si todavía no se ha dado la contraseña o han pasado mas de 2 horas, solicita la contraseña
        return render(request, 'DeCharlApp/key_form.html')
