document.addEventListener("DOMContentLoaded", function() {
    const btnOpenModal = document.getElementById("btnOpenModal");
    btnOpenModal.onclick = loadMessages();

});

function loadMessages() {
    //Obtener <div> en el que se muestran los mensajes
    const messagesContainer = document.getElementById('messages-container');

    //Obtener nombre de la sala del atributo personalizado creado en el <div>
    const roomName = messagesContainer.getAttribute('data-room-name');

    //Guardo la url para poder solicitar el json de la página actual
    const url = '/DeCharla/JsonRoom?jsonRoom=' + roomName;

    //Actualizar de mensajes
    function updateMessages() {
        fetch(url).then(response => response.json())
            .then(data => {
                //Limpiamos el contenido actual del contenedor de mensajes
                messagesContainer.innerHTML = '';

                //Agregar los nuevos mensajes al contenedor anteriormente vaciado
                data.messages.forEach(message => {
                    const messageElement = document.createElement('p');
                    //messageElement.textContent = message.text;
                    const messageText = message.text;
                    const messageDate = message.date; // Agregamos la fecha del mensaje

                    const messageAuthor = message.author;

                    // Creamos una nueva estructura HTML con el creador, fecha y contenido del mensaje
                    const messageHTML = `<span class="message-author">${messageAuthor}</span>
                    <span class="message-date">${messageDate}</span>: ${messageText}`;
                    messageElement.insertAdjacentHTML('beforeend', messageHTML);


                    messagesContainer.appendChild(messageElement);
            });
        });
    }

    //Actualizamos los mensajes cada 5 segundos
    setInterval(updateMessages, 5000)
}


