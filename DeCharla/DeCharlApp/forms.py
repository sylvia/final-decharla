
from django import forms
from .models import RoomMessage


# -- Aquí vamos a poner los formularios de la práctica.


class FormSearchRoom(forms.Form):
    roomName = forms.CharField(
        label='Nombre de la sala',
        widget=forms.TextInput(attrs={
            'id': 'roomName',
            'class': 'form-control mb-2',
            'style': 'width:90%',
            'placeholder': 'Sala',
            'required': 'required',
            'oninvalid': "this.setCustomValidity('Por favor introduzca una palabra')"
        })
    )


class FormCreateMessage(forms.ModelForm):
    class Meta:
        model = RoomMessage
        fields = ['content', 'photo']
        widgets = {
            'content': forms.Textarea(attrs={
                'id': 'inputMessage',
                'class': 'col-12',
                'cols': 40,
                'rows': 1,
            }),
            'photo': forms.CheckboxInput(attrs={
                'class': 'photoinput'
            }),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['content'].label = ''
        self.fields['photo'].label = ''
