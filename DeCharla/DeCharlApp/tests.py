# Aqui vamos a poner los test.

from django.test import TestCase, Client
from .models import Room, User, RoomMessage, Key
from http.cookies import SimpleCookie


# -- Test de extremo a extremo


class DeCharlAppTestCase(TestCase):

    def validation_and_sources(self):
        # Comprobamos el envio de la contraseña para poder acceder a cualquier recurso de la página
        c = Client()
        Key.objects.create("xx32d23")
        response = c.get('/DeCharla/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'DeCharlApp/key_form.html')

        # Hacemos envio de una nueva solicitud post indicando la contraseña de acceso
        response = c.post('/DeCharla/', {'key': 'xx32d23', 'button': 'key'})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed(response, 'DeCharlApp/showRooms.html')
        self.client.cookies = SimpleCookie({'idUSer': User.objects.filter(id=1), 'authorized': True})

        # Comprobamos la creación y posterior acceso a una sala test_room (por redirección, 302)
        response= c.post('/DeCharla/', {'roomName': 'test_room'})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed(response, 'DeCharlApp/showRoom.html')
        self.assertEqual('/DeCharla/test_room/', response.headers['Location'])

        # Comprobamos que estando dentro de una sala se mande un mensaje y nos redirija nuevamente a la sala con el
        # mensaje ya incluido
        response = c.post('/DeCharla/test_room/', {'content': 'Mensaje de prueba', 'button': 'Enviar'})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed(response, 'DeCharlApp/showRoom.html')
        self.assertEqual('/DeCharla/test_room/', response.headers['Location'])
        # El mensaje se encuentra en la sala
        content = response.content.decode('utf-8')
        self.assertIn('Mensaje de prueba', content)

        # Comprobamos que se puede solicitar la sala en formato json
        response = c.get('/DeCharla/JsonRoom/?jsonRoom=test_room')
        self.assertEqual(response.status_code,200)
        self.assertEqual('application/json', response.headers['Content-Type'])
        #Se comprueba que efectivamente el json incluye la sala y su/s mensajes
        content = response.content.decode('utf-8')
        self.assertIn('test_room', content)
        self.assertIn('Mensaje de prueba', content)

        # Comprobamos el acceso a la configuración
        response = c.get('DeCharla/Settings/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'DeCharlApp/settings.html')

        # Comprobamos el cambio de nombre a traves de un post y que volvemos a la página principal
        response = c.post('DeCharla/Settings/', {'userName': 'UserTest', 'button': 'Elegir'})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed(response, 'DeCharlApp/showRooms.html')
        # Comprobamos que el nuevo nombre de usuario se encuentra en la barra lateral actualizado
        content = response.content.decode('utf-8')
        self.assertIn('UserTest', content)

        # Configuramos el tipo de letra y el tamaño de letra.
        response = c.post('DeCharla/Settings/', {'font': 'Times New Roman', 'size': '14', 'button': 'Elegir'})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed(response, 'DeCharlApp/showRooms.html')
        # Comprobamos que está la font y el size
        content = response.content.decode('utf-8')
        self.assertIn('Times New Roman', content)
        self.assertIn('14', content)

        # Comprobamos que estando en una de las pestañas del menú (Configuración), dicha opción no aparece en él
        self.assertNotIn('Configuración', content)

        # Comprobamos que nos devuelve la página de ayuda
        response = c.get('DeCharla/Help/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'DeCharlApp/help.html')

        # Comprobamos que nos devuelve la página de admin
        response = c.get('/admin/')
        self.assertEqual(response.status_code, 200)

        #Comprobamos envio de mensajes con XML y un PUT
        xml = '''
        <?xml version="1.0" encoding="UTF-8"?>
        <messages>
            <message isimg="false">
                <text>Mi primer mensaje XML</text>
            </message>
            <message isimg="true">
                <text> Con otro más para ver si funciona</text>
            </message>
        </messages>
        '''
        response = c.put('/DeCharla/test_room/', xml)
        self.assertEqual(response.status_code,302)
        self.assertTemplateUsed('/DeCharlApp/showRoom.html')
        # Se comprueba que efectivamente el XML incluye los mensajes en la sala
        content = response.content.decode('utf-8')
        self.assertIn('Mi primer mensaje XML', content)
        self.assertIn('Con otro más para ver si funciona', content)

        #Hacemos logout
        response = c.post('/DeCharla/Logout/', {'buttonlogout': 'Logout'})
        self.assertEqual(response.status_code,302)
        self.assertTemplateUsed('/DeCharlApp/key_form.html')







