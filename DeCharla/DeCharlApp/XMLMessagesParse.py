#!/usr/bin/python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser

class HandlerMessages(ContentHandler):
    def __init__(self):
        self.inMessages = False
        self.inMessage = False
        self.inText = False
        self.messages =[]

    def startElement(self, name, attrs):
        if name == 'messages':
            self.inMessages = True
        elif self.inMessages:
            if name == 'message':
                self.currentMessage = {
                    'isimg': True if (attrs.get('isimg')=='true') else False,
                    'content': ''
                }
                self.inMessage = True
            elif name == 'text':
                self.inText = True

    def endElement(self, name):
        if name == 'messsages':
            self.inMessages = False
        elif self.inMessages:
            if name == 'message':
                self.messages.append(self.currentMessage)
                self.inMessage =  False
            elif name == 'text':
                self.inText = False

    def characters(self, chars):
        if self.inText:
            self.currentMessage['content'] += chars


class XMLContentParse:
    def __init__(self, xml_messages):
        self.parser = make_parser()
        self.manejador = HandlerMessages()
        self.parser.setContentHandler(self.manejador)
        self.parser.parse(xml_messages)
        xml_messages.close()

    def getMessages(self):
        return self.manejador.messages