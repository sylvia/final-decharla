from django.contrib import admin
from .models import Room, Key, User, RoomMessage, RoomAccess

admin.site.register(Room)
admin.site.register(Key)
admin.site.register(User)
admin.site.register(RoomMessage)
admin.site.register(RoomAccess)
