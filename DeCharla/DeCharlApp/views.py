# -- IMPORTS
import xml.etree.ElementTree as ET
import io

from django.views.decorators.csrf import csrf_exempt

from .XMLMessagesParse import XMLContentParse

from django.utils import timezone
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseNotAllowed, HttpResponseRedirect
from .models import Room, User, RoomMessage, RoomAccess
from .forms import FormSearchRoom, FormCreateMessage
from django.shortcuts import render, redirect


def parametersOfRooms():
    # -- En esta funcion lo que estamos haciendo son los bucles que nos va a indicar cuantos mensajes
    # -- De texto hay asi como cuantos mensajes de foto hay, para ponerlo en la página web, en el pie de página.

    total_messages = 0
    total_photos = 0
    rooms = Room.objects.all()

    for room in rooms:
        room_messages = RoomMessage.objects.filter(room=room)
        total_messages = total_messages + room_messages.filter(photo=0).count()
        total_photos = total_photos + room_messages.filter(photo=1).count()

    return total_messages, total_photos


def serve_mainPage(request):

    # -- A esta funcion vamos a acceder si nos han solicitado / con un GET.
    rooms = Room.objects.all()
    messages = RoomMessage.objects.all()
    user = User.objects.get(session=request.COOKIES['idUser'])
    form_SearchRoom = FormSearchRoom()
    total_messages, total_photos = parametersOfRooms()

    roomLastMessages = {}  # -- room: hola number:

    for room in rooms:
        # -- First, ya que la tabla se encuentra ordenada por fecha
        last_access = RoomAccess.objects.filter(room=room, userName=user).first()
        if last_access:
            newNumberMessages = RoomMessage.objects.filter(room=room,
                                                           messageDate__gt=last_access.lastAccessDate).count()
        else:
            # -- Cuando no ha entrado nunca a la sala le sale que tiene todos los mensajes pendientes.
            newNumberMessages = RoomMessage.objects.filter(room=room).count()

        roomLastMessages[room] = newNumberMessages

    context = {
        'rooms': rooms,
        'messages': messages,
        'user': user,
        'form_SearchRoom': form_SearchRoom,
        'total_messages': total_messages,
        'total_photos': total_photos,
        'roomLastMessages': roomLastMessages,
    }
    return render(request, 'DeCharlApp/showRooms.html', context=context)


def index_GoRoom(request) -> HttpResponse:
    # -- En esta funcion vamos a enseñar cuáles son las salas que están
    # -- creadas, muestra todas, asi como, si es un POST, nos va a llevar a una sala, tanto si no está creada como
    # -- si lo está.

    # -- Analizamos primero si es un GET
    if request.method == 'GET':
        return serve_mainPage(request)
    elif request.method == 'POST':
        return processFormRoom(request)
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def serve_roomPage(request, room):

    form_Message = FormCreateMessage()
    form_SearchRoom = FormSearchRoom()
    sala = Room.objects.get(roomName=room)
    user = User.objects.get(session=request.COOKIES['idUser'])
    message = RoomMessage.objects.filter(room=room)
    total_messages, total_photos = parametersOfRooms()

    # -- Quiero guardar el roomAccess, crear una nueva entrada cada vez que hace un get
    room_access = RoomAccess(room=sala, userName=user, lastAccessDate=timezone.now())
    room_access.save()

    context = {
        'room': sala,
        'user': user,
        'messages': message,
        'form_Message': form_Message,
        'form_SearchRoom': form_SearchRoom,
        'rooms': Room.objects.all(),
        'total_messages': total_messages,
        'total_photos': total_photos,
    }
    return render(request, 'DeCharlApp/showRoom.html', context)


def processXMLMessages(request, room):

    # -- En esta funcion lo que hacemos es si nos llega un PUT, procesa el mensaje.
    content_type = request.META.get('CONTENT_TYPE').lower()

    if content_type == 'application/xml' or content_type == 'text/xml':
        xml_body = request.body

        # -- Crear un objeto StringIO o BytesIO para almacenar los datos XML en memoria
        stream = io.BytesIO(xml_body)

        # -- Procesamos el XML
        parser = XMLContentParse(stream)
        newMessages = parser.getMessages()
        for message in newMessages:
            room = Room.objects.get(roomName=room)
            user = User.objects.get(session=request.COOKIES['idUser'])
            print(message)
            newMessage = RoomMessage.objects.create(room=room, user=user, photo=message['isimg'],
                                                    content=message['content'])

            newMessage.save()

        # Devolver una respuesta exitosa si se procesa correctamente
        return serve_roomPage(request, room.roomName)

@csrf_exempt
def showRoom(request, room):
    # -- Mostramos la sala que nos pasan por parametro.
    # -- Vamos a gestionar la llegada de un mensaje a la sala
    # -- Tanto si lo hace con un POST como si lo hace con un PUT
    # -- Si es un GET entonces simplemente mostramos la sala.

    # Yo voy a hacer get sin hacer un try-except porque sé que si o si el usuario
    # siempre va a existir si no, directamente no hubiese podido entrar a una sala y mandar
    # un mensaje (eso lo controlamos con el MiddleWare

    if request.method == 'GET':
        return serve_roomPage(request, room)
    elif request.method == 'POST':
        return createMessage(request, room)
    elif request.method == 'PUT':
        return processXMLMessages(request, room)
    else:
        return HttpResponseNotAllowed(['GET', 'POST', 'PUT'])


def settings(request):
    # -- En esta funcion vamos a especificar cuando se nos envie un POST con él
    # -- formulario de configuración, que es lo que queremos hacer.
    # -- Si se envia un GET, vamos a mostrar la plantilla de configuración.

    if request.method == 'GET':
        form_searchRoom = FormSearchRoom()
        total_messages, total_photos = parametersOfRooms()
        user = User.objects.get(session=request.COOKIES['idUser'])

        context = {
            'form_SearchRoom': form_searchRoom,
            'rooms': Room.objects.all(),
            'total_messages': total_messages,
            'total_photos': total_photos,
            'user': user,
        }
        return render(request, 'DeCharlApp/settings.html', context=context)

    elif request.method == 'POST':
        return updateUserSettings(request)

    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def page_help(request):

    # -- Pagina de ayuda, no podemos recibir nada mas que un GET.

    form_SearchRoom = FormSearchRoom()
    total_messages, total_photos = parametersOfRooms()
    user = User.objects.get(session=request.COOKIES['idUser'])
    context = {
        'user': user,
        'help': help,
        'form_SearchRoom': form_SearchRoom,
        'rooms': Room.objects.all(),
        'total_messages': total_messages,
        'total_photos': total_photos,
    }
    return render(request, 'DeCharlApp/help.html', context=context)


def createMessage(request, room):
    # -- A esta función llegamos a través de un POST, sirve para crear un mensaje.

    form = FormCreateMessage(request.POST)  # -- Esto le va a asignar cada uno de los campos que están en la request

    if form.is_valid():
        try:
            actualRoom = Room.objects.get(roomName=room)
            user = User.objects.get(session=request.COOKIES['idUser'])
            message = form.save()
            message.room = actualRoom
            message.user = user
            message.photo = True if 'photo' in request.POST else False
            message.save()

            source = '/DeCharla/' + actualRoom.roomName
            return redirect(source)

        except Room.DoesNotExist:

            return Http404('La sala no existe')

def processFormRoom(request):

    form = FormSearchRoom(request.POST)  # -- Esto le va a asignar cada uno de los campos que están en la request
    if form.is_valid():
        roomName = form.cleaned_data['roomName']
        room, created = Room.objects.get_or_create(roomName=roomName)
        source = '/DeCharla/' + room.__str__()
        return redirect(source)


def updateUserSettings(request):

    # -- Ha elegido el nombre
    if ('userName' in request.POST) and ('button' in request.POST):
        userName = request.POST['userName']
        user = User.objects.get(session=request.COOKIES['idUser'])
        user.userName = userName
        user.save()

        return serve_mainPage(request)

    # -- Ha elegido el tipo de letra además del tamaño
    elif ('font' in request.POST) and ('size' in request.POST) and ('button' in request.POST):
        font = request.POST['font']
        size = request.POST['size']
        user = User.objects.get(session=request.COOKIES['idUser'])
        user.font = font
        user.sizeFont = size
        user.save()

        return serve_mainPage(request)

    else:
        return HttpResponse('(Error 422) Petición rechazada, el formato es incorrecto.',
                            status=422, reason='Unprocessable Entity')


def room_json(request):
    if request.method == 'GET':
        if 'jsonRoom' in request.GET:
            try:
                # -- Se obtienen los mensajes de la sala indicada en el path
                room = Room.objects.get(roomName=request.GET['jsonRoom'])
                messages = RoomMessage.objects.filter(room=room)

                # -- Creación de una lista de diccionarios, donde cada diccionario guarda la info de cada mensaje
                message_list = []
                for message in messages:
                    message_info = {
                        'author': message.user.userName,
                        'text': message.content,
                        'isimg': message.photo,
                        'date': message.messageDate
                    }
                    message_list.append(message_info)

                # -- creación de un diccionario con la información de la sala, es decir nombre y mensajes
                room_info = {
                    'roomName': room.roomName,
                    'messages': message_list
                }

                return JsonResponse(room_info)

            # -- Esto realmente no va a ocurrir dado que solo podremos acceder a este recurso si nos encontramos
            # -- dentro de una sala existente
            except Room.DoesNotExist:
                return JsonResponse({'error': 'La sala no existe'})

        else:
            return HttpResponse('(Error 422) Petición rechazada, el formato es incorrecto.',
                                status=422, reason='Unprocessable Entity')
    else:
        return HttpResponseNotAllowed(['GET'])


def logout(request):

    if request.method == 'GET':
        if 'buttonlogout' in request.GET:
            user = User.objects.get(session=request.COOKIES['idUser'])
            user.delete()
            user.save()
            return HttpResponseRedirect('/DeCharla/')
    else:
        return HttpResponseNotAllowed(['GET'])
