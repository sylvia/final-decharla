# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Silvia Martinez Kolodziej
* Titulación: Doble grado en ingeniería de telecomunicaciones y ADE
* Cuenta en laboratorios: sylvia
* Cuenta URJC: s.martinezk.2018
* Video básico + Video parte opcional (Vídeo Presentación): [https://youtu.be/BWjVrFiHND4](https://youtu.be/BWjVrFiHND4)
* Despliegue (url): [Página Deslegada](http://sylvia.pythonanywhere.com/DeCharla/)
* Contraseñas: xx34d23
* Cuenta Admin Site: sylvia / 1234

## Resumen parte obligatoria
La página va a pedir una contraseña al ponerla podemos entrar en cualquier recurso de la página. esto es gracias a un middleware que no nos deja metermos a ningun recurso si no ponemos la contraseña.

Una vez dentro nos va a salir un listado de las salas que ya están creadas si no hay ninguna sala entonces nos va a aparecer que no hay ninguna sala, si no nos gustan las salas que hay a la derecha de la página nos encontramos una barra la cual la podemos usar para crear una sala así como acceder a la sala que queramos si existe. En la página principal además nos indica los mensajes totales que hay dentro de la sala asi como los mensajes que nos faltan por leer en dicha sala, siendo igual al numero total si nunca nos hemos metido en la sala. 

Una vez en dentro de la sala, ahí nos encontramos los mensajes que hay de otras personas o nuestros mensajes, con la fecha y el usuario que lo ha creado, si nunca nadie ha escrito en la sala entonces nos indicará que no hay mensajes. Podemos incluir una foto o un mensaje de texto, si queremos hacer tramapas y decir que hemos puesto una foto cuando no hemos incluido ninguna foto entonces nos idicará que no se ha incluido ninguna foto asi como un simbolo de imagen rota. Arriba de la sala, tenemos dos botones, uno de ellos es para ver la sala en formato json, y la otra es para ver la sala dinámica, esta sala dinámica no tiene ningun recurso extra es simplemente un modal de boostrap, esperamos 30 segundos y aparecen los mensajes actualizados. Los mensajes que se van escribiendo estan en orden de llegada es decir que la parte de arriba salen los mensjes mas nuevos y la parte de abajo los mas viejos.

Podemos irnos a configuración, en este lado podemos elegir el nombre con el que queremos que aparezca tanto en la barra lateral derecha como en los mensajes que escribamos, asi como la configuracion del tipo de letra y el tamaño de la letra que queremos usar en el chat de las diferentes salas, este valor se puede cambiar las veces que uno quiera. 

Tambien podemos ir a la pagina de ayuda donde nos indicará como hay que funcionar en la pagina ademas de datos curiosos de como funciona y datos de la creadora de la pagina. 

En la barra de arriba no va a salir aquel boton donde estamos: por ejemplo: si estamos en configuracion en la barra de arriba no va a salir el boton de configuracion. 

Tenemos un pie de página donde salen algunas estadisticas de las salas, como el número de mensajes (de texto) totales, asi como el número de salas totales y el número de imagenes totales publicadas. 

## Lista partes opcionales

* Nombre parte: favicon. He incluido el favicon de la pagina, que es el simbolo que representa toda la pagina, un loro.
* Nombre parte: El chat, cuando habla el que escribe sale en el lado derecho del recuadro del chat asi como si es otro usuario el que 
esta escribiendo entonces sale de otro color y en el lado izquierdo. 
* Nombre parte: La sala dinámica no requiere de un recurso extra si no esta hecho a través de un modal, es decir una vez le das al boton 'sala dinámica' esta no se va a un recurso externo si no que sale un chat (a través del uso de Modal bs) que al esperar 5 segundos nos va actualizando el contenido. 
* Nombre parte: Tests extremo a extremo de todos los recursos. 
* Nombre parte: logout hay un boton que al darle nos cierra la sesion. 
* Nombre parte: Se cumple con toda la parte responsive de cualquier parte de la página, de forma que la barra menú se vuelve un desplegable en dispositivos pequeños, y por otro lado la barra lateral pasa a estar bajo el menú anteriormente mencionado. El resto de componentes de cada uno de los recursos se encuentra "responsive". 
